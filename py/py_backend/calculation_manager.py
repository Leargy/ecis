from py.py_backend.utils import calculate
from py.py_backend.utils.data.records import ResultSet


class Manager:

    def __init__(self, model):
        self.cells = model.cells
        self.y = int(model.global_data['y'][0])
        self.n = float(model.global_data['n'][0])
        self.f = [int(a) for a in model.global_data['f']]
        self.results = []

    def run(self):
        tmp = []
        for f in self.f:
            for cell in self.cells:
                import numpy as np
                imp_arr = np.abs(
                    calculate.impedance_pac(self.y, self.n, float(f), cell.cc, cell.rm, cell.rc, cell.rx, cell.rs))
                tmp.append(ResultSet(cell.name, float(f), imp_arr, cell.t))
            self.results.append(tmp)
            tmp = []
        return self.results
