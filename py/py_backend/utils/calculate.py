import math
import numpy as np


def impedance(y, n, f, cc, rm, rc, rx, rs):
    j = complex(0, 1)
    cd = (j * f) ** (n*-1) / y
    fj = f*j
    return 2 * (1/fj*cd + rm + (2/cc*fj + rc + rx) / (rx * (2/fj*cc + rc))) + rs


def impedance_0(y, n, f, rs):
    j = complex(0, 1)
    cd = (j * f) ** (-n) / y
    fj = f * j
    return 1 / (fj*cd + 1) + rs


def ci(zo, z):
    return math.fabs(z - zo) / math.fabs(zo)


def impedance_pac(y, n, f, cc_arr, rm_arr, rc_arr, rx_arr, rs_arr):
    arr = np.array([])
    i = len(cc_arr)
    for k in range(i):
        arr = np.append(arr, impedance(y, n, f, cc_arr[k], rm_arr[k], rc_arr[k], rx_arr[k], rs_arr[k]))
    return arr


def impedance_0_pac(y, n, f, rs_arr):
    arr = np.array([])
    i = len(rs_arr)
    for k in range(i):
        arr = np.append(arr, impedance_0(y, n, f, rs_arr[k]))
    return arr


def ci_pac(zo_arr, z_arr):
    arr = np.array([])
    i = len(z_arr)
    for k in range(i):
        arr = np.append(arr, math.fabs(z_arr[k] - zo_arr[k]) / math.fabs(zo_arr[k]))
    return arr
