class DataSet:
    def __init__(self, name, cc, rm, rc, rx, rs, t):
        self.name = name
        self.cc = cc
        self.rm = rm
        self.rc = rc
        self.rx = rx
        self.rs = rs
        self.t = t


# class State:
#     def __init__(self, y, n):
#         self.y = y
#         self.n = n
#         self.dt = 2
#         self.to = 21
#         self.tn = 43
#
#
# class GlobalState:
#     def __init__(self, y, n, to, dt, boxes):
#         self.y = y
#         self.n = n
#         self.dt = 2
#         self.to = 21
#         self.tn = 43


class ResultSet:
    def __init__(self, name, f, imp_arr, t_arr):
        self.name = name
        self.f = f
        self.imp_arr = imp_arr
        self.t_arr = t_arr
