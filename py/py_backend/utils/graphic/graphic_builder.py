import matplotlib.pyplot as plt


class GraphBuilder:
    def __init__(self, path=''):
        self.fig = None
        self.path = path

    def init_figure(self):
        self.fig = plt.figure()

    def form_simple_plot(self, resultSet):
        if self.fig:
            for set in resultSet:
                plt.plot(set.t_arr, set.imp_arr, label=set.name)

    def export(self, file_name, x_name = "", y_name = ""):
        if self.fig:
            plt.legend()
            plt.savefig(self.path + file_name)
            plt.close(self.fig)
