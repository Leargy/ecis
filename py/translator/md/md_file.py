from py.translator.structure.Model import Model
from py.py_backend.utils import DataSet


class MdFile:
    def __init__(self, file_name):
        self.simply_data = None
        self.boxes = None
        self.file_name = file_name

    def parse(self):
        self.boxes = []
        self.simply_data = {}
        name = "Blank"
        cc = []
        rm = []
        rc = []
        rx = []
        rs = []
        t = []

        stage = 0
        with open(self.file_name, 'r') as file:
            for line in file:
                elements = line.strip().split()
                if elements:
                    if stage == 0:
                        if elements[0] == "NAME":
                            name = ''.join(elements[1:])
                            stage = 1
                            continue
                        self.simply_data.update({elements[0]: elements[1:]})
                        continue
                    if stage == 1:
                        if elements[0] == "NAME":
                            self.boxes.append(DataSet(name, cc, rm, rc, rx, rs, t))
                            cc = []
                            rm = []
                            rc = []
                            rx = []
                            rs = []
                            t = []
                            name = ''.join(elements[1:])
                            continue
                        cc.append(float(elements[0]))
                        rm.append(float(elements[1]))
                        rc.append(float(elements[2]))
                        rx.append(float(elements[3]))
                        rs.append(float(elements[4]))
                        t.append(float(elements[5]))
        if cc:
            self.boxes.append(DataSet(name, cc, rm, rc, rx, rs, t))

    def get_model(self):
        return Model(self.simply_data, self.boxes)
