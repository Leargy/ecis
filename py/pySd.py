def read_file(file_path):
    from py.translator.md.md_file import MdFile
    from py.py_backend.calculation_manager import Manager

    md_file_obj = MdFile(file_path)
    md_file_obj.parse()
    model = md_file_obj.get_model()

    return Manager(model)