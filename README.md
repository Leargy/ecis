# ECIS

## Structure
- py_backend folder contains utils, record structures and main - sequention logic.
- py_backend/utils provides you with "calculate.py" file with math equations
- py_backend/utils/graphix contains module with graphic building fonctions


## Getting started

At this stage we assume that all important variables are got from experiment and stored in file.
To path vars in programm the following structure must be followed:

```
n value
y value
f values

NAME Normal cells
Cc Rm Rc Rx Rs t
...
```
### Usage
```
    model = pySd.read_file(data_file_path)
    model.run()
```


You will get List of data sets which contains cell name, calculated impedance in format of complex number and followed temperature value.
