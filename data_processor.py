import sys

from py import pySd
from py.py_backend.utils.graphic.graphic_builder import GraphBuilder


def main() -> int:
    if len(sys.argv) == 2:
        path = sys.argv[1]
        model = pySd.read_file(path)
        res = model.run()
        builder = GraphBuilder()
        i = 0
        for sub_res in res:
            builder.init_figure()
            builder.form_simple_plot(sub_res)
            builder.export("my_cool_name"+str(i))
            i+=1

    return 0


if __name__ == '__main__':
    sys.exit(main())
